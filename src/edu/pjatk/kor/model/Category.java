package edu.pjatk.kor.model;

import java.util.ArrayList;
import java.util.List;

public class Category extends BaseEntity {
	private List<Manufacture> manufacture= new ArrayList<Manufacture>();
	
	public Category(int id, String name, List<Manufacture> manufacture){
		super(id, name);		
		this.manufacture=manufacture;
	}
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id=id;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String n){
		this.name=n;
	}
	
	public List<Manufacture> getManufacture(){
		return manufacture;
	}
	
	public void setManufacture(List<Manufacture> manufacture){
		this.manufacture=manufacture;
	}
	
	@Override
	public String toString() {
		return "Category { id="+id+", name="+name+", manufactures="+manufacture+"}";
	}
}
