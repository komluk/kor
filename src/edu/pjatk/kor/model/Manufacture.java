package edu.pjatk.kor.model;

import java.util.List;

public class Manufacture extends BaseEntity {
	private Address address;
	
	public Manufacture(int id, String name, Address address){
		super(id, name);
		this.address=address;
	}	
	
	public Address getAddress(){
		return address;
	}
	
	public void setAddress(Address address){
		this.address=address;
	}
	
	@Override
	public String toString() {
		return "Manufacture{id="+id+", name="+name+", address="+address+"}";
	}
}
