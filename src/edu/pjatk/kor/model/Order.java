package edu.pjatk.kor.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {
	private int orderNumber;
	private boolean paid;
	private Date executionDate;
	private Date shipmentDate;
	private Client client;
	private List<Product> products= new ArrayList<Product>();
	
	public Order(int orderNumber, boolean paid, Date executionDate, Date shipmentDate, Client client, List<Product> products){
		super();
		this.orderNumber=orderNumber;
		this.paid =paid;
		this.executionDate=executionDate;
		this.shipmentDate=shipmentDate;
		this.client=client;
		this.products=products;
	}
	
	public int getNumber(){
		return orderNumber;
	}
	
	public void setNumber(int orderNumber){
		this.orderNumber=orderNumber;
	}
	
	public boolean getPaid(){	
		return paid;
	}
	
	public void setPaid(boolean paid){
		this.paid=paid;
	}
	
	public Date getExecutionDate(){
		return executionDate;
	}
	
	public void setExecutionDate(Date exectutionDate){
		this.executionDate=exectutionDate;
	}
	
	public Date getShipmentDate(){
		return shipmentDate;
	}
	
	public void setShipmentDate(Date shipmentDate){
		this.shipmentDate=shipmentDate;
	}
	
	public Client getClient(){
		return client;
	}
	
	public void setClient(Client client){
		this.client=client;
	}
	
	public List<Product> getProducts(){
		return products;
	}
	
	public void setProducts(List<Product> products){
		this.products=products;	
	}
	
	@Override
	public String toString() {
		return "Order{orderNumber="+orderNumber+", paid="+paid+", executionDate="+executionDate+", shipmentDate="+shipmentDate+", client="+client+", products="+products+"}";
	}
}
