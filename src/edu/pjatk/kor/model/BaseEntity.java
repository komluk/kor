package edu.pjatk.kor.model;

public class BaseEntity {
	protected int id;
	protected String name;
	
	public BaseEntity(int id, String name){
		super();
		this.id=id;
		this.name=name;
	}
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id=id;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String n){
		this.name=n;
	}
}
