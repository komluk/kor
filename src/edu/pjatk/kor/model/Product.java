package edu.pjatk.kor.model;

import java.util.ArrayList;
import java.util.List;

public class Product extends BaseEntity {
	private double price;
	private Category category;
	private List<Order> orders=new ArrayList<Order>();
	
	public Product(int id, String name, double price, Category category, List<Order> orders){
		super(id,name);
		this.price=price;
		this.category=category;
		this.orders=orders;
	}
	
	public double getPrice(){
		return price;
	}
	
	public void setPrice(double price){
		this.price=price;
	}
	
	public Category getCategory(){
		return category;
	}
	
	public void setCategory(Category category){
		this.category=category;
	}
	
	public List<Order> getOrders(){
		return orders;
	}
	
	public void setOrders(List<Order> orders){
		this.orders=orders;
	}
	
	@Override
	public String toString() {
		return "Product{id="+id+", name="+name+", price="+price+", category="+category+", orders="+orders+"}";
	}
}
