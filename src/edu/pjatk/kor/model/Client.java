package edu.pjatk.kor.model;

public class Client {
	private String nip;
	private String companyName;	
	private String regon;
	private String surname;
	private String name;
	private Address address;
	
	public Client(){}
	
	public Client(String companyName, String nip, String regon, String surname, String name, Address address){
		super();
		this.companyName=companyName;
		this.nip=nip;
		this.regon=regon;
		this.surname=surname;
		this.name=name;
		this.address=address;
	}
	
	//
	public String getCompanyName(){
		return companyName;
	}
	
	public void setCompanyName(String companyName){
		this.companyName=companyName;
	}
	
	public String getNip(){
		return nip;
	}
	
	public void setNip(String n){
		this.nip=n;
	}
	
	public String getRegon(){
		return regon;
	}
	
	public void setRegon(String r){
		this.regon=r;
	}
	
	public String getSurname(){
		return surname;
	}
	
	public void setSurname(String s){
		this.surname=s;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String n){
		this.name=n;
	}
	
	public Address getAddress(){
		return address;
	}
	
	public void setAddress(Address address){
		this.address=address;
	}
	
	@Override
	public String toString() {
		return "Client { nip="+nip+", companyName="+companyName+", regon="+regon+", surname="+surname+", name="+name+", address="+address+"}";
	}
}
