package edu.pjatk.mode.data;

import java.io.File;
import java.text.*;
import java.util.*;

import com.db4o.*;
import com.db4o.config.Configuration;
import com.db4o.config.ObjectClass;
import com.db4o.ext.ExtObjectContainer;

import edu.pjatk.kor.model.*;

public class ExampleData {
	
	public static final String DB_FILENAME = "korData.db";
	private ObjectContainer dbConn;
	private Random random;
	
//	private List<Address> address;
	private List<Category> category;
	private List<Client> client;
	private List<Manufacture> manufacture;
	private List<Order> order;
	private List<Product> product;
		
	public List<Client> getClients(){
		return client;
	}
	
	public List<Manufacture> getManufactures(){
		return manufacture;
	}
	
	public List<Category> getCategories(){
		return category;
	}
	
	public List<Order> getOrders(){
		return order;
	}
	
	public List<Product> getProducts(){
		return product;
	}
	
	public ExampleData(){
		initData();
		saveData();
	}
	
	public static void main(String[] args) {
		System.out.println("Example Data");
		
		ExampleData data= new ExampleData();
		System.out.println(data.category);
		System.out.println(data.client);
		System.out.println(data.manufacture);
	}
	
	public ObjectContainer getConnection(){
		if(dbConn==null){
			File dbFile= new File(DB_FILENAME);
			dbFile.delete();
			dbConn=Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), DB_FILENAME);
		}
		return dbConn;
	}
	
	private void initData(){			
		
		List<Date> sampleDates = new ArrayList<Date>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			sampleDates.add(df.parse("2016-03-01"));
			sampleDates.add(df.parse("2015-09-22"));
			sampleDates.add(df.parse("2014-01-06"));
			sampleDates.add(df.parse("2013-07-11"));
			sampleDates.add(df.parse("2012-04-30"));
			sampleDates.add(df.parse("2011-06-15"));
			sampleDates.add(df.parse("2010-09-30"));
			sampleDates.add(df.parse("2000-12-20"));
			sampleDates.add(df.parse("2001-01-01"));
			sampleDates.add(df.parse("2002-06-14"));
		} catch (ParseException e) {
			System.err.println(e);
		}
		List<String> cities = new ArrayList<String>();
		cities.add("Łódź");
		cities.add("Warszawa");
		cities.add("Poznań");
		cities.add("Wrocław");
		cities.add("Kraków");
		cities.add("Gdańsk");
		cities.add("Szczecin");
		cities.add("Rzeszów");
		cities.add("Katowice");
		
		List<String> streets = new ArrayList<String>();
		streets.add("5'th avenue");
		streets.add("S. La Salle St.");
		streets.add("South Cass Avenue");
		streets.add("Park Avenue");
		streets.add("West Avenue");
		streets.add("North Avenue");

		List<String> femaleLNames = new ArrayList<String>();
		femaleLNames.add("NOWAK");
		femaleLNames.add("KOWALSKA");
		femaleLNames.add("WIŚNIEWSKA");
		femaleLNames.add("WÓJCIK");
		femaleLNames.add("KOWALCZYK");
		femaleLNames.add("KAMIŃSKA");
		femaleLNames.add("LEWANDOWSKA");
		femaleLNames.add("ZIELIŃSKA");
		femaleLNames.add("SZYMAŃSKA");
		femaleLNames.add("WOŹNIAK");
		femaleLNames.add("DĄBROWSKA");
		femaleLNames.add("KOZŁOWSKA");
		femaleLNames.add("JANKOWSKA");
		femaleLNames.add("MAZUR");
		femaleLNames.add("WOJCIECHOWSKA");
		femaleLNames.add("KWIATKOWSKA");
		femaleLNames.add("KRAWCZYK");
		femaleLNames.add("PIOTROWSKA");
		femaleLNames.add("KACZMAREK");
		femaleLNames.add("GRABOWSKA");
		femaleLNames.add("PAWŁOWSKA");
		femaleLNames.add("MICHALSKA");
		femaleLNames.add("ZAJĄC");
		femaleLNames.add("KRÓL");
		femaleLNames.add("JABŃOŃSKA");
		femaleLNames.add("WIECZOREK");
		femaleLNames.add("NOWAKOWSKA");
		femaleLNames.add("WRÓBEL");
		femaleLNames.add("MAJEWSKA");
		femaleLNames.add("OLSZEWSKA");
		femaleLNames.add("STĘPIEŃ");
		femaleLNames.add("JAWORSKA");
		femaleLNames.add("MALINOWSKA");
		femaleLNames.add("ADAMCZYK");
		femaleLNames.add("NOWICKA");
		femaleLNames.add("GÓRSKA");
		femaleLNames.add("DUDEK");
		femaleLNames.add("PAWLAK");
		femaleLNames.add("WITKOWSKA");
		femaleLNames.add("WALCZAK");
		femaleLNames.add("RUTKOWSKA");
		femaleLNames.add("SIKORA");
		femaleLNames.add("BARAN");
		femaleLNames.add("MICHALAK");
		femaleLNames.add("SZEWCZYK");
		femaleLNames.add("OSTROWSKA");
		femaleLNames.add("TOMASZEWSKA");
		femaleLNames.add("PIETRZAK");
		femaleLNames.add("JASIŃSKA");
		femaleLNames.add("WRÓBLEWSKA");

		List<String> femaleFNames = new ArrayList<String>();

		femaleFNames.add("ANNA");
		femaleFNames.add("MARIA");
		femaleFNames.add("KATARZYNA");
		femaleFNames.add("MAŁGORZATA");
		femaleFNames.add("AGNIESZKA");
		femaleFNames.add("KRYSTYNA");
		femaleFNames.add("BARBARA");
		femaleFNames.add("EWA");
		femaleFNames.add("ELŻBIETA");
		femaleFNames.add("ZOFIA");
		femaleFNames.add("JANINA");
		femaleFNames.add("TERESA");
		femaleFNames.add("JOANNA");
		femaleFNames.add("MAGDALENA");
		femaleFNames.add("MONIKA");
		femaleFNames.add("JADWIGA");
		femaleFNames.add("DANUTA");
		femaleFNames.add("IRENA");
		femaleFNames.add("HALINA");
		femaleFNames.add("HELENA");
		femaleFNames.add("BEATA");
		femaleFNames.add("ALEKSANDRA");
		femaleFNames.add("MARTA");
		femaleFNames.add("DOROTA");
		femaleFNames.add("MARIANNA");
		femaleFNames.add("GRAŻYNA");
		femaleFNames.add("JOLANTA");
		femaleFNames.add("STANISŁAWA");
		femaleFNames.add("IWONA");
		femaleFNames.add("KAROLINA");
		femaleFNames.add("BOŻENA");
		femaleFNames.add("URSZULA");
		femaleFNames.add("JUSTYNA");
		femaleFNames.add("RENATA");
		femaleFNames.add("ALICJA");
		femaleFNames.add("PAULINA");
		femaleFNames.add("SYLWIA");
		femaleFNames.add("NATALIA");
		femaleFNames.add("WANDA");
		femaleFNames.add("AGATA");
		femaleFNames.add("ANETA");
		femaleFNames.add("IZABELA");
		femaleFNames.add("EWELINA");
		femaleFNames.add("MARZENA");
		femaleFNames.add("WIESŁAWA");
		femaleFNames.add("GENOWEFA");
		femaleFNames.add("PATRYCJA");
		femaleFNames.add("KAZIMIERA");
		femaleFNames.add("EDYTA");
		femaleFNames.add("STEFANIA");

		List<String> maleLNames = new ArrayList<String>();
		maleLNames.add("NOWAK");
		maleLNames.add("KOWALSKI");
		maleLNames.add("WIŚNIEWSKI");
		maleLNames.add("WÓJCIK");
		maleLNames.add("KOWALCZYK");
		maleLNames.add("KAMIŃSKI");
		maleLNames.add("LEWANDOWSKI");
		maleLNames.add("ZIELIŃSKI");
		maleLNames.add("WOŹNIAK");
		maleLNames.add("SZYMAŃSKI");
		maleLNames.add("DĄBROWSKI");
		maleLNames.add("KOZŁOWSKI");
		maleLNames.add("JANKOWSKI");
		maleLNames.add("MAZUR");
		maleLNames.add("WOJCIECHOWSKI");
		maleLNames.add("KWIATKOWSKI");
		maleLNames.add("KRAWCZYK");
		maleLNames.add("KACZMAREK");
		maleLNames.add("PIOTROWSKI");
		maleLNames.add("GRABOWSKI");
		maleLNames.add("ZAJĄC");
		maleLNames.add("PAWŁOWSKI");
		maleLNames.add("KRÓL");
		maleLNames.add("MICHALSKI");
		maleLNames.add("WRÓBEL");
		maleLNames.add("WIECZOREK");
		maleLNames.add("JABŁOŃSKI");
		maleLNames.add("NOWAKOWSKI");
		maleLNames.add("MAJEWSKI");
		maleLNames.add("STĘPIEŃ");
		maleLNames.add("OLSZEWSKI");
		maleLNames.add("JAWORSKI");
		maleLNames.add("MALINOWSKI");
		maleLNames.add("DUDEK");
		maleLNames.add("ADAMCZYK");
		maleLNames.add("PAWLAK");
		maleLNames.add("GÓRSKI");
		maleLNames.add("NOWICKI");
		maleLNames.add("SIKORA");
		maleLNames.add("WALCZAK");
		maleLNames.add("WITKOWSKI");
		maleLNames.add("BARAN");
		maleLNames.add("RUTKOWSKI");
		maleLNames.add("MICHALAK");
		maleLNames.add("SZEWCZYK");
		maleLNames.add("OSTROWSKI");
		maleLNames.add("TOMASZEWSKI");
		maleLNames.add("PIETRZAK");
		maleLNames.add("ZALEWSKI");
		maleLNames.add("WRÓBLEWSKI");

		List<String> maleFNames = new ArrayList<String>();
		maleFNames.add("JAN");
		maleFNames.add("ANDRZEJ");
		maleFNames.add("PIOTR");
		maleFNames.add("KRZYSZTOF");
		maleFNames.add("STANISŁAW");
		maleFNames.add("TOMASZ");
		maleFNames.add("PAWEŁ");
		maleFNames.add("JÓZEF");
		maleFNames.add("MARCIN");
		maleFNames.add("MAREK");
		maleFNames.add("MICHAŁ");
		maleFNames.add("GRZEGORZ");
		maleFNames.add("JERZY");
		maleFNames.add("TADEUSZ");
		maleFNames.add("ADAM");
		maleFNames.add("ŁUKASZ");
		maleFNames.add("ZBIGNIEW");
		maleFNames.add("RYSZARD");
		maleFNames.add("DARIUSZ");
		maleFNames.add("HENRYK");
		maleFNames.add("MARIUSZ");
		maleFNames.add("KAZIMIERZ");
		maleFNames.add("WOJCIECH");
		maleFNames.add("ROBERT");
		maleFNames.add("MATEUSZ");
		maleFNames.add("MARIAN");
		maleFNames.add("RAFAŁ");
		maleFNames.add("JACEK");
		maleFNames.add("JANUSZ");
		maleFNames.add("MIROSŁAW");
		maleFNames.add("MACIEJ");
		maleFNames.add("SŁAWOMIR");
		maleFNames.add("JAROSŁAW");
		maleFNames.add("KAMIL");
		maleFNames.add("WIESŁAW");
		maleFNames.add("ROMAN");
		maleFNames.add("WŁADYSŁAW");
		maleFNames.add("JAKUB");
		maleFNames.add("ARTUR");
		maleFNames.add("ZDZISŁAW");
		maleFNames.add("EDWARD");
		maleFNames.add("MIECZYSŁAW");
		maleFNames.add("DAMIAN");
		maleFNames.add("DAWID");
		maleFNames.add("PRZEMYSŁAW");
		maleFNames.add("SEBASTIAN");
		maleFNames.add("CZESŁAW");
		maleFNames.add("LESZEK");
		maleFNames.add("DANIEL");
		maleFNames.add("WALDEMAR");
		
		random= new Random();
		client= new ArrayList<Client>();
		manufacture= new ArrayList<Manufacture>();
		category= new ArrayList<Category>();
		product= new ArrayList<Product>();
		order= new ArrayList<Order>();
		
		for(String c: cities){
			for(int i=1; i<5; i++){
				Address address= new Address(c, randomZip(),random(streets));
				client.add(new Client("Company"+i, "NIP-"+randomInt(100,200), "REG-"+randomInt(100,200), random(maleLNames), random(maleFNames), address));			
			}
			
			for(int i=1; i<10; i++){
				Address address= new Address(c,randomZip(),random(streets));
				manufacture.add(new Manufacture(randomInt(100,200), "Manufacture"+i, address));
			}
			
			for(int i=1; i<5; i++){
				category.add(new Category(randomInt(100,200), "Category"+i, getManufactures()));
			}
			
			for(Category cat: category){
				product.add(new Product(randomInt(100,200), "Product"+randomInt(100,200), random.nextDouble(), cat, getOrders()));
				
			}
		}
		
		for(Date d: sampleDates){
			for(Client c: client){
				order.add(new Order(randomInt(100,200), randomBoolean(), d, d, c, pickRandom(getProducts(), 2)));	
			}
		}		
		
	}
	
	private void saveData(){
    	ExtObjectContainer con = getConnection().ext();
    	
    	ObjectSet<Category> categ= con.query(Category.class);
    	if(categ.isEmpty()) {
    		for(Category c : this.category) {
    			con.store(c);
    		}
    	}
    	ObjectSet<Manufacture> man= con.query(Manufacture.class);
    	if(man.isEmpty()) {
    		for(Manufacture m : this.manufacture) {
    			con.store(m);
    		}
    	}
    	ObjectSet<Client> clients = con.query(Client.class);
    	if(clients.isEmpty()) {
    		for(Client c : this.client) {
    			con.store(c);
    		}
    	}	
    	con.commit();
	}
	
	private <T> T random(List<T> col) {
		int randomIndex = (int)(Math.random()*col.size());
		return col.get(randomIndex);
	}
	
	private String randomZip() {
        int r = (int)(Math.random() * 100000);
        DecimalFormat df = new DecimalFormat("00000");
        StringBuilder sb = new StringBuilder(df.format(r));
        sb.insert(2, "-");
        return sb.toString();
	}
	
	private int randomInt(int min, int max) {
		return (int)(Math.random() * (max-min+1)) + min;
	}
	
	private Date randomDate() {
		return randomDate(1900, 2000);
	}
	
	private Date randomDate(int minYear, int maxYear) {
		int year = randomInt(minYear, maxYear);
        int month = randomInt(0, 11);

        GregorianCalendar gc = new GregorianCalendar(year, month, 1);

        int day = randomInt(1, gc.getActualMaximum(gc.DAY_OF_MONTH));

        gc.set(year, month, day);

        return gc.getTime();
	}
	
	
	public <T> List<T> pickRandom(List<T> list, int n){
		List<T> copy = new LinkedList<T>(list);
	    Collections.shuffle(copy);
	    return copy.subList(0, n);
	}
	
	private boolean randomBoolean() {
		int r = (int)(Math.random() * 2);
		return r > 0 ? true : false;
	}
}
 